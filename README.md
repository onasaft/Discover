# Discover - a web directory generator 
Discover is a web directory generator to promote personal site discovery such that everyone owns their data.

In this repository, you will find the main client, though others have and can make their own. It's a shell script which builds your discovery directory from a json file hosted by you. This script is periodically called by a cronjob, which updates said page. Then nginx serves this page statically. If one of your contacts decides to go incognito, or simply change their profile picture, these changes will be reflected without any effort on your part.

## Follow these steps to become discoverable:

1) Grab the [example json file](https://codeberg.org/onasaft/Discover/src/branch/master/.well-known/discover.json)
2) [Check out the docs](https://codeberg.org/onasaft/Discover/src/branch/master/Docs/README.md)
3) Modify it with your data
4) Serve that file from **your.website/.well-known/discover.json**

### How do I show previews on my site?

1) Install dependencies: `apt install -yy jq curl`
2) Clone this repo
3) Copy the [discover.sh](discover.sh) script to your webroot:
4) `chmod +x discover.sh`
5) Run `./discover.sh -h` to see scripting options. You can manually add your site in the script if you don't wish to use the `-s` flag
6) Make a new cronjob: `crontab -e`
7) `0 */2 * * * /bin/sh -c 'cd /var/www/your.website; ./discover.sh'`

This will refresh your page at the start of every 2nd hour each day. Visit [cron.help](https://cron.help) to help
find an update rate which is slightly different than the example to lessen bandwidth strain. Please make sure your directory is being updated at least once a day.

The cronjob will build your page but if you don't want to wait, run the discover script locally (use the remote url option) and upload it to your server.

Now go to `your.site/discover` (or whatever's the name of your client's page)
You should see your json file displayed as a web directory, as well as previews of other people's sites you chose to connect with.

**It's recommended to use a json linter before publishing your data**

Repeat this process if you have more than 1 website. Now you're done!

### Can I embed my "discover" page in my links page?
Yes! One hacky way is to use an [iframe](Docs/example.html).

## Credits
Idea came from [Interverse](https://codeberg.org/gabe/Interverse)

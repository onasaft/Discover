#!/bin/sh
#
# Dependencies: jq, curl
# Optional Programs: openring https://git.sr.ht/~sircmpwn/openring

# Tip for Hugo test server users: comment out the following line then run this
# script as "./discover -z | tac | tac > [Hugo directory]/static/discover/index.html".
# That way the script will write only once and Hugo won't auto-reload the page
# repeatedly while it runs.

#Your friends rss feeds for Openring
friend_feeds=$(
cat << EOF
https://example.org/rss.xml
https://example.org/rss.xml
https://example.org/rss.xml
EOF
)

# Handling for allowing non-Discover-enabled sites, overriding the output file, and printing a help message.
helpMsg() {
echo "Discover - a web directory generator to promote personal site discovery.
usage:
-f              Force inclusion of sites that do not support Discover
-h              Print this help message
-i <path>       Specify JSON file for input
-I <url>        Specify remote JSON for input
-o <path>       Specify output file default is ./index.html
-O              Print to standard output without writing anything to disk
-s <url>        URL of your website format: (\"https://your.site\")"
exit
}
# Handling for script options
while getopts "s:fi:I:o:Oh" flag; do
    case $flag in
        s) site="$OPTARG";;
        f) force=1;;
        i) json="$(cat "$OPTARG")";;
        I) json="$(curl -s "$OPTARG")";;
        o) file="$OPTARG";;
        O) stdout=1;;
        h|?) helpMsg;;
    esac
done

# Set default URL if not specified
if [ -z "$site" ]; then
    # Replace this with your web address (omit trailing slash):
    site="https://your.site"
fi

# Set default JSON file if not specified
if [ -z "$json" ]; then
    json=$(curl -s "$site""/.well-known/discover.json")
fi

# Set default output file if not specified
if [ -z "$file" ]; then
    if [ ! -d ./discover ]; then
        mkdir discover
    fi
    file="discover/index.html"
fi

# Redirect standard output to the output file if not specified
if [ -z "$stdout" ]; then
    exec 1>"$file"
fi

preview()
{
    if response=$(curl -sf "$1/.well-known/discover.json") || response=$(curl -sf "$1/.well-known/discover") ; then
        # You need to have a client
        client=$(echo "$response" | jq -r '.client') && [ -z "$client" ] && return
        # Print the thumbnail preview
        echo "$response" | jq -r  '"<li><a title=\"" + .about + "\" href=\"" + .client + "\" target=\"_top\"><img src=\""' 2> /dev/null
        imageurl=$(echo "$response" | jq -r '.image')
        imagename=$(echo "$response" | jq -r '.location' | sed 's;.*//;;' | tr -d '/').$(echo $imageurl | sed 's;.*\.;;')
        # Cache preview image locally
        curl -s $imageurl > $(dirname $file)/$imagename
        echo -n "$imagename\"></a>"
        # Print a direct link to the preview's website with its identifiable name
        echo "$response" | jq -r '"<a href=\"" + .location + "\" target=\"_top\" >" + "<strong>" + .name + "</strong></a></li>"'
    else
        if [ -z $force ]; then
            echo "\"$url\" does not appear to support Discover. Ignoring for now, add it to your static links page or override with \"-f\"." 1>&2
        else
            echo "<li><a href="$1" target=\"_top\">"$1"</a></li>"
        fi
    fi
}

echo "<!DOCTYPE html>
<head>
    <title>Discover</title>
    <meta charset=\"utf-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
    <meta name=\"robots\" content=\"noindex, nofollow\">
    <style>
        :root {
                --fg: black;
                --bg: white;
                --href: royalblue;
                --hover: navy;
                --seen: blueviolet;
        }
        body {
                display: flex;
                font: 12pt/1.4 sans-serif;
                color: var(--fg);
                background: var(--bg);
        }
        nav {
                float: left;
                min-width: 12rem;
        }
        nav img {
                max-width: 12rem;
                max-height: 12rem;
        }
        nav, main {
                padding: 1rem;
        }
        main img {
                max-width: 10rem;
                max-height: 10rem;
        }
        main ul {
                display: flex;
                flex-direction: row;
                align-items: flex-end;
                flex-wrap: wrap;
                justify-content: flex-start;
        }
        main li {
                padding: 1rem;
                display: flex;
                flex-direction: column;
        }
        main li a {text-align:center}
        article {max-width: 38ch}
        h1 a {font-size: medium}
        a {color: var(--href);text-decoration:none}
        a:hover {color: var(--hover)}
        a:visited{color: var(--seen)}
        @media (prefers-color-scheme: dark) {
                :root {
                        --fg: snow;
                        --bg: #000;
                        --href: lightskyblue;
                        --hover: azure;
                }
        }
        /* openring styling */
        .webring .articles {
          padding-left: 2.5em;
          display: flex;
          flex-wrap: wrap;
        }
        .webring .title {
          margin: 0;
        }
        .webring .article {
          flex: 1 1 0;
          display: flex;
          flex-direction: column;
          margin: 0.5rem;
          padding: 0.5rem;
                border: 1px solid gray;
                padding: 0.5em;
          min-width: 10rem;
          max-width: 12rem;
        }
        .webring .summary {
          font-size: 0.8rem;
          flex: 1 1 0;
        }
        .webring .attribution {
          text-align: right;
          font-size: 0.8rem;
        }
    </style>
</head><body>"

# Host's Data
name=$(echo "$json" | jq -r '.name')
homepage=$(echo "$json" | jq -r '.location')

echo "
<a href="$homepage" target="_top">$name &#8617;</a>
<main>
"

# Preview Connections
echo "$json" | jq -r '.preview_connections | to_entries | map([.key] + .value) | map(join(",")) | join("\n")' | while read -r line; do
    name=$(echo "$line" | cut -d ',' -f1 )
    echo "<h3>$name</h3>"
    echo "<ul>"
    for url in $(echo "$line" | cut -f2- -d ',' | tr ',' ' '); do
        preview "$url"
    done
    echo "</ul>"
done

which openring > /dev/null && openring -s $(echo "$friend_feeds" | sed -e ':a;N;$!ba;s/\n/ \-\s /g') -n 5 < in.html
# in.html -> https://codeberg.org/onasaft/Discover/src/branch/master/Docs/in.html

echo "<em>Powered by <a href="https://onasaft.codeberg.page/discover" target="_top">Discover</a></em></body>"
